using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalPref : MonoBehaviour
{

    public const string COLOR_SELECT = "ColorSelect";
    [SerializeField]
    public List<MaterialAnimal> materialsAnimal = new List<MaterialAnimal>();

    public Color32 debugColor;

    public void ChangeColorMesh(Color32 color, string nameMat)
    {
        foreach(MaterialAnimal mat in materialsAnimal)
        {
            if(mat.mat_name == nameMat)
            {
                mat.mesh.materials[mat.ID].SetColor(COLOR_SELECT, color);
            }
        }
    }

}

[System.Serializable]
public struct MaterialAnimal
{
    public string mat_name;
    public int ID;
    public MeshRenderer mesh;
}
