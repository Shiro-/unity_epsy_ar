using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Animal
{
    public string aName;

    public GameObject prefab3D;

    public AnimalPref prefabScript;

    public List<BodyAnimal> bodyAnimals = new List<BodyAnimal>();
}

[System.Serializable]
public class BodyAnimal
{
    public string name_mat;
    public Color32 color;
    public Texture2D maskBody;
    //public Material mat;
}