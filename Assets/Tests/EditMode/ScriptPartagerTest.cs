using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ScriptPartagerTest
    {
        [Test]
        public void itShouldSetReseau()
        {
            //Given
            var underTest = new ScriptPartager();
            string reseau = "Facebook";
            //When
            underTest.SetReseau(reseau);
            //Then
            Assert.AreEqual(underTest.reseau, reseau);
        }

        [Test]
        public void itShouldGetContact()
        {
            //Given
            var underTest = new ScriptPartager();
            string reseau = "test@test.com";
            //When
            var contact = underTest.GetContact(reseau);
            //Then
            Assert.AreNotEqual(contact, null);
        }

        [Test]
        public void itShouldGetNullContact()
        {
            //Given
            var underTest = new ScriptPartager();
            string reseau = "fakeemail@null.com";
            //When
            var contact = underTest.GetContact(reseau);
            //Then
            Assert.AreEqual(contact, null);
        }

        [Test]
        public void itShouldReturnEmptyNameError()
        {
            //Given
            var underTest = new ScriptPartager();
            string name = "";
            string email = "test@test.com";
            string hashtag = "Super outil !";
            underTest.reseau = "Facebook";
            //When
            underTest.SendProspect(name, email, hashtag);
            //Then
            Assert.AreEqual(underTest.errorMessage, "Veuillez introduire votre prénom.");
        }

        [Test]
        public void itShouldReturnEmptyEmailError()
        {
            //Given
            var underTest = new ScriptPartager();
            string name = "Test";
            string email = "";
            string hashtag = "Super outil !";
            underTest.reseau = "Facebook";
            //When
            underTest.SendProspect(name, email, hashtag);
            //Then
            Assert.AreEqual(underTest.errorMessage, "Veuillez introduire votre email.");
        }

        [Test]
        public void itShouldReturnInvalidEmailError()
        {
            //Given
            var underTest = new ScriptPartager();
            string name = "Test";
            string email = "test@";
            string hashtag = "Super outil !";
            underTest.reseau = "Facebook";
            //When
            underTest.SendProspect(name, email, hashtag);
            //Then
            Assert.AreEqual(underTest.errorMessage, "Veuillez introduire un email valide.");
        }

        [Test]
        public void itShouldReturnEmptyHashtagError()
        {
            //Given
            var underTest = new ScriptPartager();
            string name = "Test";
            string email = "test@test.com";
            string hashtag = "";
            underTest.reseau = "Facebook";
            //When
            underTest.SendProspect(name, email, hashtag);
            //Then
            Assert.AreEqual(underTest.errorMessage, "Veuillez introduire votre hashtag.");
        }

        [Test]
        public void itShouldReturnEmptyReseauError()
        {
            //Given
            var underTest = new ScriptPartager();
            string name = "Test";
            string email = "test@test.com";
            string hashtag = "Super outil !";
            underTest.reseau = "";
            //When
            underTest.SendProspect(name, email, hashtag);
            //Then
            Assert.AreEqual(underTest.errorMessage, "Veuillez choisir un réseau sur le quel partager votre capture.");
        }

        [Test]
        public void itShouldPostWhenSendProspect()
        {
            //Given
            var underTest = new ScriptPartager();
            string name = "Test";
            string email = "test2@test.com";
            string hashtag = "Mignon !";
            underTest.reseau = "Instagram";
            //When
            underTest.SendProspect(name, email, hashtag);
            //Then
            Assert.AreEqual(underTest.succesfullMessage, "Votre capture a bien été partagée !");
        }

        [Test]
        public void itShouldPatchWhenSendProspect()
        {
            //Given
            var underTest = new ScriptPartager();
            string name = "Test";
            string email = "test@test.com";
            string hashtag = "J'adore !";
            underTest.reseau = "Twitter";
            //When
            underTest.SendProspect(name, email, hashtag);
            //Then
            Assert.AreEqual(underTest.succesfullMessage, "Votre capture a bien été partagée !");
        }

    }
}
