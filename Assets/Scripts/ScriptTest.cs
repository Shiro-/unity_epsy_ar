using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScriptTest : MonoBehaviour
{

    public GameObject cube;
    public GameObject Nico;

    public TMP_InputField myInput;

    public float speed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //cube.transform.position = new Vector3((cube.transform.position.x + speed) * Time.deltaTime , 0, 0);
        
        cube.transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
    }

    public void SayHello(string msg)
    {
        Debug.Log("Hello world : " + myInput.text);
    }
}
